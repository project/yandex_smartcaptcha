<?php

namespace Drupal\yandex_smartcaptcha\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\yandex_smartcaptcha\YandexSmartCaptcha;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The class implements the Yandex SmartCaptcha configuration form.
 */
class YandexSmartCaptchaSettingsForm extends ConfigFormBase implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The Yandex SmartCaptcha object.
   *
   * @var \Drupal\yandex_smartcaptcha\YandexSmartCaptcha
   */
  protected $smartCaptcha;

  /**
   * Constructs an UpdateSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\yandex_smartcaptcha\YandexSmartCaptcha $smartCaptcha
   *   The Yandex SmartCaptcha service object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, YandexSmartCaptcha $smartCaptcha) {
    parent::__construct($config_factory);
    $this->smartCaptcha = $smartCaptcha;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('yandex_smartcaptcha.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'yandex_smartcaptcha_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['yandex_smartcaptcha.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('yandex_smartcaptcha.settings');

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Yandex SmartCaptcha integration'),
      '#description' => $this->t('If disabled Yandex SmartCaptcha element are not displayed and not verified. (Except Test form page!)'),
      '#default_value' => $config->get('enabled'),
    ];

    $form['api_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Yandex SmartCaptcha API url'),
      '#description' => $this->t('Yandex SmartCaptcha API url'),
      '#default_value' => $config->get('api_url'),
    ];

    $env_loaded = $this->smartCaptcha->isEnvVarsExists();
    $form['site_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site key'),
      '#default_value' => $config->get('site_key'),
      '#description' => $this->t('The site key of SmartCaptcha. Check how to <a href=":url">create Yandex SmartCaptcha</a>.', [
        ':url' => 'https://cloud.yandex.ru/docs/smartcaptcha/operations/create-captcha',
      ]),
      '#required' => !$env_loaded,
      '#disabled' => $env_loaded,
      '#placeholder' => $env_loaded ? $this->t('Site Key loaded from .env file') : '',
    ];

    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret key'),
      '#default_value' => $config->get('secret_key'),
      '#description' => $this->t('The secret key of SmartCaptcha. Check how to <a href=":url">create Yandex SmartCaptcha</a>.', [
        ':url' => 'https://cloud.yandex.ru/docs/smartcaptcha/operations/create-captcha',
      ]),
      '#required' => !$env_loaded,
      '#disabled' => $env_loaded,
      '#placeholder' => $env_loaded ? $this->t('Secret Key loaded from .env file') : '',
    ];

    $form['invisible'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable invisible mode'),
      '#default_value' => $config->get('invisible'),
      '#description' => $this->t('Enable invisible mode of Yandex SmartCaptcha'),
    ];

    $form['test'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable test mode'),
      '#default_value' => $config->get('test'),
      '#description' => $this->t('Enable test mode of Yandex SmartCaptcha'),
    ];

    $form['log'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Logging'),
      '#description' => $this->t('Log successful Yandex SmartCaptcha responses'),
      '#default_value' => $config->get('log'),
    ];

    $form['button_hide_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Submit button hide mode'),
      '#description' => $this->t('Choose the type of submit button hiding.'),
      '#options' => ['hide' => $this->t('Hide'), 'disable' => $this->t('Disable')],
      '#default_value' => $config->get('button_hide_mode') ?? 'hide',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('yandex_smartcaptcha.settings')
      ->set('enabled', $values['enabled'])
      ->set('log', $values['log'])
      ->set('invisible', $values['invisible'])
      ->set('test', $values['test'])
      ->set('button_hide_mode', $values['button_hide_mode'])
      ->set('api_url', $values['api_url'])
      ->set('site_key', $values['site_key'])
      ->set('secret_key', $values['secret_key'])
      ->save();
    parent::submitForm($form, $form_state);
  }

}
