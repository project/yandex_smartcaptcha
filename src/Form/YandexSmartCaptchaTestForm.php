<?php

namespace Drupal\yandex_smartcaptcha\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\yandex_smartcaptcha\YandexSmartCaptcha;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The class implements the test form for Yandex SmartCaptcha integration.
 */
class YandexSmartCaptchaTestForm extends FormBase {

  /**
   * The yandex_smartcaptcha.service service.
   *
   * @var \Drupal\yandex_smartcaptcha\YandexSmartCaptcha
   */
  protected $smartCaptcha;

  /**
   * Class constructor.
   */
  public function __construct(YandexSmartCaptcha $smartCaptcha) {
    $this->smartCaptcha = $smartCaptcha;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('yandex_smartcaptcha.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'yandex_smartcaptcha_test_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['smartcaptcha_element'] = [
      '#type' => 'smartcaptcha_element',
      '#smartcaptcha' => [],
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Exit if SmartCaptcha is disabled.
    if (!$this->smartCaptcha->isEnabled()) {
      return;
    }

    $this->messenger()
      ->addStatus($this->t('Yandex SmartCaptcha successfully verified!'));
  }

}
