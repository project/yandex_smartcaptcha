<?php

namespace Drupal\yandex_smartcaptcha\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\yandex_smartcaptcha\Element\YandexSmartCaptchaElement;

/**
 * Webform submission handler plugin.
 *
 * @WebformHandler(
 *   id = "smartcaptcha_element",
 *   label = @Translation("SmartCaptcha Element"),
 *   category = @Translation("smartcaptcha_element"),
 *   description = @Translation("Adds SmartCaptcha protection to the webform."),
 *   cardinality =
 *   \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_IGNORED,
 *   submission =
 *   \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class SmartCaptchaElementWebformHandler extends WebformHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'element_name' => NULL,
      'smartcaptcha' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['#after_build'][] = [static::class, 'afterBuildConfigurationForm'];

    $form['element_name_override'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Override the element name.'),
      '#description' => $this->t('When checked, a custom element name can be provided for the hidden input element used to store SmartCaptcha tokens, otherwise the system name of this handler will be used.'),
    ];

    $form['element_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Element name'),
      '#pattern' => '^[a-z0-9_]*$',
      '#description' => $this->t('Unique element name. Please enter only lower-case letters, numbers and underscores.'),
    ];

    $form['smartcaptcha_defaults'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use SmartCaptcha default settings.'),
      '#description' => $this->t('When checked, the SmartCaptcha default settings will be used.'),
      '#default_value' => empty($this->configuration['smartcaptcha']),
    ];

    $form['smartcaptcha'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('SmartCaptcha'),
      '#tree' => TRUE,
    ];

    $form['smartcaptcha'] = YandexSmartCaptchaElement::buildConfigurationForm($form['smartcaptcha'], $this->configuration['smartcaptcha'] ?? []);

    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * After build callback for the configuration form.
   */
  public static function afterBuildConfigurationForm(array $form, FormStateInterface $form_state) {
    $element_name_override = sprintf(':input[name="%s"]', $form['element_name_override']['#name']);
    $element_name_override_checked = [
      $element_name_override => ['checked' => TRUE],
    ];

    $form['element_name']['#states']['enabled'] = $element_name_override_checked;
    $form['element_name']['#states']['visible'] = $element_name_override_checked;

    $captcha_defaults = sprintf(':input[name="%s"]', $form['smartcaptcha_defaults']['#name']);
    $captcha_defaults_checked = [
      $captcha_defaults => ['checked' => TRUE],
    ];

    $form['smartcaptcha']['#states']['disabled'] = $captcha_defaults_checked;
    $form['smartcaptcha']['#states']['invisible'] = $captcha_defaults_checked;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValues();

    $this->configuration['element_name'] = empty($values['element_name_override']) && !empty($values['element_name'])
      ? $values['element_name']
      : NULL;
    $this->configuration['smartcaptcha'] = empty($values['smartcaptcha_defaults']) ? $values['smartcaptcha'] : [];
  }

  /**
   * {@inheritdoc}
   */
  public function alterForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    $element_name = empty($this->configuration['element_name'])
      ? $this->getHandlerId()
      : $this->configuration['element_name'];

    $form[$element_name] = [
      '#type' => 'smartcaptcha_element',
      '#smartcaptcha' => $this->configuration['smartcaptcha'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $settings = $this->getSettings();
    $details = '<br><b>' . $this->t('Used default settings') . '</b>';
    // Collect overriden SmartCaptcha options list.
    if (!empty($settings['smartcaptcha'])) {
      $details = [];
      foreach ($settings['smartcaptcha'] as $key => $value) {
        $details[] = sprintf('<b>%s</b>: %s', $key, $value);
      }
      $details = '<br>' . implode('<br>', $details);
    }

    return [
      '#markup' => $this->t("Yandex SmartCaptcha element handler") . $details,
    ];
  }

}
