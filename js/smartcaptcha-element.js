/**
 * The file contains the Yandex SmartCaptcha integration with the forms.
 */

/**
 * Callback for SmartCaptcha initialization.
 */
function smartCaptchaInit() {
  if (window.smartCaptcha) {
    delayedCallback(smartCaptchaInitCallback);
  }
}

/**
 * Delayed callback of given function.
 */
function delayedCallback(callback, waitCount) {
  waitCount = waitCount || 0;
  // First, wait for grecaptcha to be loaded.
  const container = document.getElementById('container-smartcaptcha');
  if (container == null) {
    waitCount = waitCount || 0;
    // But only wait for a maximum of 2 seconds.
    if (waitCount > 20) {
      callback();
    }
    else {
      setTimeout(delayedCallback.bind(null, callback, ++waitCount), 100);
    }
  }
  else {
    callback();
    return true;
  }
}

/**
 * Render of SmartCaptcha widget element.
 */
function smartCaptchaInitCallback() {
  const container = document.getElementById('container-smartcaptcha');
  Drupal.SmartCaptcha = {
    sitekey: container.getAttribute('data-sitekey'),
    hl: container.getAttribute('data-hl'),
    invisible: container.hasAttribute('data-invisible'),
    test: container.hasAttribute('data-test'),
    button_hide_mode: container.getAttribute('data-button_hide_mode'),
    callback: callback
  };
  const widgetId = window.smartCaptcha.render(container, Drupal.SmartCaptcha);
}

/**
 * SmartCaptcha after verification callback handler.
 */
function callback(token) {
  var captcha = document.getElementById('smartcaptcha-element-id');
  captcha.setAttribute('value', token);
  var form = captcha.closest('form');
  var submit = form.querySelector(".smartcaptha-submit-button[type=submit]");

  // Automatically submit form if used invisible mode and token is valid.
  if (Drupal.SmartCaptcha.invisible && token) {
    if (Drupal.SmartCaptcha.ajax_validated) {
      // For ajax forms.
      submit.click();
    }
    else {
      // For regular forms.
      form.submit();
    }
  }

  if (Drupal.SmartCaptcha.button_hide_mode == 'disable') {
    submit.removeAttribute('disabled');
  }
  else {
    submit.style.display = 'block';
  }
}


(function ($) {
  'use strict';

  // Handle ajax forms.
  // By wrapping the actual jQuery.ajaxSubmit() function we can delay ajax form
  // submits until recaptcha elements have been provisioned.
  if ($.fn && $.fn.ajaxSubmit) {
    var originalAjaxSubmit = $.fn.ajaxSubmit;

    $.fn.ajaxSubmit = function () {
      var self = this;
      var args = arguments;
      var triggered = args[0].data['_triggering_element_name'];
      // Call original submit handler if called not primary submit button.
      if (!Drupal.SmartCaptcha.invisible || triggered !== 'op') {
        originalAjaxSubmit.apply(self, args);
        return;
      }

      // On first ajax form submit just call Smartcaptcha validation challenge.
      if (!Drupal.SmartCaptcha.ajax_validated) {
        Drupal.SmartCaptcha.ajax_validated = true;
        window.smartCaptcha.execute();
      }
      else {
        // On second ajax form submit if captcha was validated run form base
        // ajax submit handler.
        Drupal.SmartCaptcha.ajax_validated = false;
        originalAjaxSubmit.apply(self, args);
      }
    };
  }


  Drupal.behaviors.YandexSmartCaptcha = {
    attach: function attach(context, settings) {
      // Loop throw the all SmartCaptcha elements.
      $(once('container-smartcaptcha', '#container-smartcaptcha', context)).each(function () {
        var invisible = settings.SmartCaptcha.invisible;
        var button_hide_mode = settings.SmartCaptcha.button_hide_mode || 'hide';

        const $form = $(this).parents('form').first();
        // Find the primary submit button.
        var $submit = $form.find('[type="submit"].button--primary').last();
        // If not found, get the last submit button.
        if (!$submit.length) {
          $submit = $form.find('[type="submit"]').last();
        }
        $submit.addClass('smartcaptha-submit-button');

        if (!invisible) {
          if (button_hide_mode === 'hide') {
            $submit.hide();
          }
          else {
            $submit.prop('disabled', true)
          }
        }

        $form.on('submit', function (e) {
          if (!window.smartCaptcha) {
            return;
          }

          // Execute SmartCaptcha verification for invisible mode.
          if (Drupal.SmartCaptcha.invisible) {
            window.smartCaptcha.execute();
            return false;
          }
        });
      });
    }
  }

}(jQuery));
